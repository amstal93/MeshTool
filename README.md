##About

Mesh Tool GUI
Generate mesh from surface/curves. Developed on linux.


##Mesh Process

## Mesh from text contours

1. Loads a stack of curves from a text file
2. Writes the curves as polygons to a .vtk file (X.curves.vtk)
3. Estimates/orients normals using CGAL
4. Uses CGAL to reconstruct the surface from the oriented points
5. Writes the surface mesh to a .vtk file (X.surf.vtk)
6. Uses CGAL to create a volume mesh
7. Writes the volume mesh to a .vtk file (X.mesh.vtk)
Caution: the mesh should be written in VTK binary format to preserve precision if it will be used with FullMonte

## MeshMulti from multiple text contours

1. Loads a series of text-file curves
2. Applies some hacky heuristics to fix up the curves
3. Writes the curves to R.curve.vtk file for each region R
4. Checks for duplicate points (should not have!)
5. Writes individual surfaces to R.surf.vtk files for each region R
6. Writes individual meshes to R.mesh.vtk for each region R
7. Writes a single labelled mesh (scalars denote region code) to a VTK binary file

## Mesh from vtk surface file (polydata)

1. Read the surface from vtk polydata
2. Use vtk triangle filter to keep only the triangle surface
3. Smooth the surface using vtkSmoothFilter
4. Writes to CGAL polyhedron
5. Close the surface, filling any holes
6. Mesh the volume into tetras and output to mesh.vtk

## Mesh multi from multiple vtk surface files
1. Loads a series of vtk surface files
2. Use tirangle filter to every surface and smoothe every surface
3. Check every surface is closed
3. Transfer all vtk surfaces to CGAL polyhedron
4. Writes a single labelled mesh (Using a self-write multi polyhedron domain wrapper) to a VTK binary file


##Usage
## Paramters

1. facet_angle: This parameter controls the shape of surface facets. Actually, it is a lower bound for the angle (in degree) of surface facets. When boundary surfaces are smooth, the termination of the meshing process is guaranteed if the angular bound is at most 30 degrees.

2. facet_size: This parameter controls the size of surface facets. Actually, each surface facet has a surface Delaunay ball which is a ball circumscribing the surface facet and centered on the surface patch. The parameter facet_size is either a constant or a spatially variable scalar field, providing an upper bound for the radii of surface Delaunay balls.

3. facet_distance: This parameter controls the approximation error of boundary and subdivision surfaces. Actually, it is either a constant or a spatially variable scalar field. It provides an upper bound for the distance between the circumcenter of a surface facet and the center of a surface Delaunay ball of this facet.

4. cell_radius_edge_ratio: This parameter controls the shape of mesh cells (but can't filter slivers, as we discussed earlier). Actually, it is an upper bound for the ratio between the circumradius of a mesh tetrahedron and its shortest edge. There is a theoretical bound for this parameter: the Delaunay refinement process is guaranteed to terminate for values of cell_radius_edge_ratio bigger than 2.

5. cell_size: This parameter controls the size of mesh tetrahedra. It is either a scalar or a spatially variable scalar field. It provides an upper bound on the circumradii of the mesh tetrahedra.

6. smooth factor: the number of iterations fro smooth process. Bigger the number, smoother the surface. But it will also decrease the volume of object,please be very careful with this. A good setting will be 1000

7. BBox Margin(x,y,z): the increase size of bounding box 

8. Table: Order 0 is the innermost layer. Offset x,y,z is used the move a certain part with small offset.

## Tools other than mesh

1. Mesh quality assess: Generate a quality report for the entire mesh and every single region

2. Mesh Compare tool: Compare two mesh and calculate their similarity in volume

3. Visualize: Visualize the result. And can use clippe to see the inside quality of tetrahedron


## Install

Mesh GUI builds depend on 
1. VTK-7.1.1 

2. CGAL

3. boost (>=1.58.0)

4. cmake(>=3.4)

5. Qt5, can be installed from apt-get 

6. Eigen library

For all the installation details, please refer to Dockerfile 
