#ifndef FILEDIALOG_H
#define FILEDIALOG_H

#include <QDialog>
#include <vector>
#include <string>
#include <QStringList>

namespace Ui {
class FileDialog;
}

class FileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FileDialog(QWidget *parent = 0);
    ~FileDialog();

    static std::vector<std::string> GetInputFile(QWidget*);

    static QStringList filePaths;

private slots:
    //Save the path information to file paths
    void on_buttonBox_accepted();

    //Clear the path information to file paths
    void on_buttonBox_rejected();

    //Add (an) extra file(s) to text window
    void on_addButton_clicked();

private:
    Ui::FileDialog *ui;
};

#endif // FILEDIALOG_H
