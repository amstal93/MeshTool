 /*
 * surface_crop.cpp
 *
 *  Created on: Feb 23, 2018
 *      Author: jcassidy
 */

#include <vector>

#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>

#include <vtkSphere.h>
#include <vtkBox.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkClipPolyData.h>

#include <boost/filesystem.hpp>

#include <boost/range/adaptor/indexed.hpp>

#include <sstream>

using namespace std;

/** Read a delimited array from an input stream.
 * Returns true on success, false on failure.
 */

template<typename T,size_t N>bool read_array_with_delim(istream& is,char delim,array<T,N>& a)
{
	char c;
	for(unsigned i=0;i<N;++i)
	{
		is >> a[i];

		if (is.fail())
			return false;

		if (i < N-1)
			is >> c;

		if (c != delim)
			return false;
	}
	return true;
}

int main(int argc,char **argv)
{
	// define command-line options
	boost::program_options::options_description cmdline;

	cmdline.add_options()
			("aabb",boost::program_options::value<string>(),"Crop to axis-aligned bounding box specified as xmin,ymin,zmin:xmax,ymax,zmax")
			("sphere",boost::program_options::value<string>(),"Crop to sphere specified as centre and radius    x,y,z:radius")
			("surface-files",boost::program_options::value<vector<string>>()->multitoken(),"Surface file names (including .vtk extension)")
			("output-suffix",boost::program_options::value<string>(),"Output suffix XXX.vtk -> XXX.<suffix>.vtk")
			("help,h","Display help message");

	// define positional options (file names)
	boost::program_options::positional_options_description surfaceFilesOption;
	surfaceFilesOption.add("surface-files",-1);

	// parse command line
	boost::program_options::basic_command_line_parser<char> parser(argc,argv);

	parser.options(cmdline);
	parser.positional(surfaceFilesOption);

	boost::program_options::variables_map vm;

	boost::program_options::store(parser.run(),vm);
	boost::program_options::notify(vm);


	// handle options
	if (vm.count("help"))
	{
		cout << cmdline << endl;

		cout << endl << "Remaining arguments are the surface mesh files to be processed" << endl;
		//cout << surfaceFilesOption << endl;
	}

	unsigned nRegionDef = vm.count("aabb")+vm.count("sphere");

	if (nRegionDef == 0)
	{
		cout << "Must specify a crop region" << endl;
		return 1;
	}
	else if (nRegionDef > 1)
	{
		cout << "Only one crop region definition is allowed." << endl;
		return 1;
	}

	vtkImplicitFunction* clipFunction=nullptr;

	if (vm.count("aabb"))
	{
		string arg = vm["aabb"].as<string>();
		cout << "Using axis-aligned bounding box" << endl;
		cout << "  Argument string: '" << arg << '\'' << endl;

		array<float,3> bbmin,bbmax;
		char delim;

		bool ok=true;

		stringstream ss(arg);

		ok &= read_array_with_delim(ss,',',bbmin);
		ss >> delim;
		ok &= delim == ':';
		ok &= read_array_with_delim(ss,',',bbmax);

		if (!ok)
		{
			cout << "Failed to parse aabb definition" << endl;
			return 1;
		}

		vtkBox* box = vtkBox::New();
		box->SetBounds(bbmin[0],bbmax[0],bbmin[1],bbmax[1],bbmin[2],bbmax[2]);

		clipFunction = box;
	}
	else if (vm.count("sphere"))
	{
		cout << "Using sphere" << endl;
		cout << "  Argument string: '" << vm["sphere"].as<string>() << endl;

		bool ok = true;
		char delim;

		stringstream ss(vm["sphere"].as<string>());

		array<float,3> centre;
		float radius;

		ok &= read_array_with_delim(ss,',',centre);
		ss >> delim;
		ok &= delim==':';
		ss >> radius;

		if (!ok)
		{
			cout << "Failed to parse sphere definition" << endl;
			return 1;
		}

		vtkSphere* sph = vtkSphere::New();
		sph->SetRadius(radius);
		sph->SetCenter(centre[0],centre[1],centre[2]);

		clipFunction=sph;
	}

	if (!clipFunction)
	{
		cout << "No clip function defined! Bailing out" << endl;
		return 1;
	}



	// set up common pipeline (read, clip, write)
	vtkPolyDataReader* reader = vtkPolyDataReader::New();

	vtkClipPolyData* clipper = vtkClipPolyData::New();
		clipper->SetInputConnection(reader->GetOutputPort());
		clipper->InsideOutOn();
		clipper->SetClipFunction(clipFunction);

	vtkPolyDataWriter* writer = vtkPolyDataWriter::New();
		writer->SetInputConnection(clipper->GetOutputPort());

	vector<string> surfaceFiles = vm["surface-files"].as<vector<string>>();
	string suffix;

	if (vm.count("output-suffix"))
		suffix = vm["output-suffix"].as<string>();
	else
		suffix = ".crop";

	bool err=false;

	// loop over files, clipping as specified
	for(const auto fn : surfaceFiles | boost::adaptors::indexed(0U))
	{
		cout << '[' << fn.index() << "] file '" << fn.value() << "'" << endl;

		boost::filesystem::path inputPath(fn.value());

		if (inputPath.extension() == ".vtk")
		{
			reader->SetFileName(inputPath.native().c_str());
			writer->SetFileName((inputPath.stem().native() + suffix +".vtk").c_str());
		}
		else
		{
			cout << "  Expecting extension .vtk. Skipping." << endl;
			err=true;
			continue;
		}

		writer->Update();

		if (reader->GetOutput()->GetNumberOfCells()==0)
		{
			cout << "  ERROR: 0 cells in input" << endl;
			err=true;
		}
	}

	return err;
}

