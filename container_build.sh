## Script for building
# Source & build folders follow Gitlab-CI convention for Docker runner

SRC_DIR=/builds/FullMonte/MeshTool
BUILD_DIR=/builds/FullMonte/MeshTool/Build/Release
INSTALL_DIR=/usr/local/MeshTool

mkdir -p $BUILD_DIR \
 && cd $BUILD_DIR \
 && cmake -G Ninja \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
        -DVTK_DIR=/usr/local/VTK-7.1.1/lib/cmake/vtk-7.1 \
        $SRC_DIR \
 && ninja -j4 \
 && ninja install
