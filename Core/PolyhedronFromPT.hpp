/*
 * PolyhedronFromPT.hpp
 *
 *  Created on: Mar 20, 2016
 *      Author: jcassidy
 */

#ifndef POLYHEDRONFROMPT_HPP_
#define POLYHEDRONFROMPT_HPP_

#include <vector>
#include <array>

#include <CGAL/Polyhedron_incremental_builder_3.h>

/** Wraps a const& to points and tetras to construct a polyhedron */

template<class HDS>class PolyhedronFromPT : public CGAL::Modifier_base<HDS>
{
public:
	PolyhedronFromPT(const std::vector<std::array<float,3>>& P,const std::vector<std::array<unsigned,3>>& T) :
		m_P(P),
		m_T(T){}

	void operator()(HDS& hds);

private:
	const std::vector<std::array<float,3>>& 	m_P;
	const std::vector<std::array<unsigned,3>>& 	m_T;
};

template<class HDS>void PolyhedronFromPT<HDS>::operator()(HDS& hds)
{
	typedef typename HDS::Vertex Vertex;
	typedef typename Vertex::Point Point;
	CGAL::Polyhedron_incremental_builder_3<HDS> builder(hds,true);

    builder.begin_surface(m_P.size(),m_T.size()-1);

	for(unsigned i=0;i<m_P.size();++i)
		builder.add_vertex(Point(m_P[i][0],m_P[i][1],m_P[i][2]));

    for(unsigned i=1;i<m_T.size();++i)
	{
		builder.begin_facet();
		for(unsigned j=0;j<3;++j)
			builder.add_vertex_to_facet(m_T[i][j]);
		builder.end_facet();
	}

    if ( builder.check_unconnected_vertices()) {
        if ( ! builder.remove_unconnected_vertices()) {

            std::cerr << " " << std::endl;
            std::cerr << "Polyhedron_scan_OFF<Traits>::" << std::endl;
            std::cerr << "operator()(): input error: cannot "
                         "successfully remove isolated vertices."
                      << std::endl;
            builder.rollback();
            return;
        }
    }

	builder.end_surface();
}


#endif /* POLYHEDRONFROMPT_HPP_ */
