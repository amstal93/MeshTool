#ifndef POLYHEDRAL_MESH_DOMAIN_WITH_CUSTOM_SEEDS_HPP
#define POLYHEDRAL_MESH_DOMAIN_WITH_CUSTOM_SEEDS_HPP

#include <vector>

#include <boost/foreach.hpp>

#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/grid_simplify_point_set.h>

template<class Polyhedron, class K>
struct Polyhedral_mesh_domain_with_custom_seeds : public CGAL::Polyhedral_mesh_domain_3<Polyhedron, K> {

  typedef typename Polyhedron::Point_3 Point_3;

  std::vector<Point_3> seeds;

  Polyhedral_mesh_domain_with_custom_seeds(const Polyhedron& polyhedron, double cell_size)
    : CGAL::Polyhedral_mesh_domain_3<Polyhedron, K>(polyhedron),
      seeds(polyhedron.points_begin(), polyhedron.points_end()) {

    seeds.erase(CGAL::grid_simplify_point_set(seeds.begin(), seeds.end(), cell_size),
                seeds.end());
  }

  struct Construct_initial_points
  {
    Construct_initial_points(const std::vector<Point_3>& seeds)
      : seeds(seeds)
    {}

    template<class OutputIterator>
    OutputIterator operator()(OutputIterator pts, const int n = 8) const
    {
      BOOST_FOREACH(Point_3 p, seeds){
        *pts++ = std::make_pair(p,0);
      }
      return pts;
    }

  private:
    const std::vector<Point_3>& seeds;
  };

  Construct_initial_points construct_initial_points_object() const
  {
    return Construct_initial_points(seeds);
  }

};

#endif
