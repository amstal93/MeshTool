/*
 * CGALMeshToVTKUnstruturedGrid.hpp
 *
 *  Created on: Jan 15, 2016
 *      Author: jcassidy
 */

#ifndef CGALMESHTOVTKUNSTRUCTUREDGRID_HPP_
#define CGALMESHTOVTKUNSTRUCTUREDGRID_HPP_

#include <vtkUnstructuredGrid.h>
#include <vtkIntArray.h>
#include <vtkIdTypeArray.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>

#include <boost/range.hpp>
#include <boost/functional/hash.hpp>

#include <unordered_map>
#include <array>

#include "CGALTriangulationToPT.hpp"

template<typename Triangulation3>vtkUnstructuredGrid* CGALMeshToVTKUnstructuredGrid(const Triangulation3& M)
{
	////// Range of cells & vertices
    boost::iterator_range<typename Triangulation3::Finite_cells_iterator> cells =
            boost::iterator_range<typename Triangulation3::Finite_cells_iterator>(
                    M.finite_cells_begin(),
                    M.finite_cells_end());

//	boost::iterator_range<typename Triangulation3::Finite_vertices_iterator> vertices =
//			boost::iterator_range<typename Triangulation3::Finite_vertices_iterator>(
//					M.finite_vertices_begin(),
//					M.finite_vertices_end());


    ////// Create VTK arrays

    size_t Nt = M.number_of_finite_cells();

    vtkIdTypeArray* vtkID = vtkIdTypeArray::New();
    vtkID->SetNumberOfTuples(Nt*5);


    size_t Np = M.number_of_vertices();

    vtkPoints* vtkP = vtkPoints::New();
    vtkP->SetNumberOfPoints(Np);



    ////// Create dynamic array of points with hash to vertex handle to ID

    std::unordered_map<std::array<double,3>,vtkIdType,boost::hash<std::array<double,3>>> pointToIDMap;


    vtkIdType i=0;		// Index into cell index array

    for (const auto cell : cells)
    {
//		vtkID->SetTuple1(i++,4);
        vtkID->SetValue(i++,4);
        for(unsigned j=0;j<4;++j)
        {
            const auto vh = cell.vertex(j);
            std::array<double,3> p{ vh->point()[0], vh->point()[1], vh->point()[2] };
            vtkIdType id;

            const auto insertResult = pointToIDMap.insert(make_pair(p,pointToIDMap.size()));

            id = insertResult.first->second;
            assert(id < Np);

            if (insertResult.second)
            {
                vtkP->SetPoint(id,insertResult.first->first.data());
                assert(id == insertResult.first->second);
            }

//			vtkID->SetTuple1(i++,id);
            vtkID->SetValue(i++,id);
        }
    }

//    vector<array<float,3> > P;
//    vector<array<unsigned,4> > T;

//    tie(P,T) = CGALTriangulationToPT(M);


//        vtkIdTypeArray* ids = vtkIdTypeArray::New();
//            ids->SetNumberOfTuples(5*T.size());

//        unsigned k=0;
//        for(unsigned i=0;i<T.size();++i)
//        {
//            ids->SetValue(k++,4);
//            for(unsigned j=0;j<4;++j)
//                ids->SetValue(k++,T[i][j]);
//        }

//        vtkCellArray* ca = vtkCellArray::New();
//            ca->SetCells(T.size(), ids);

//        vtkPoints* vtkP = vtkPoints::New();
//            vtkP->SetNumberOfPoints(P.size());

//        for(unsigned i=0;i<P.size();++i)
//            vtkP->SetPoint(i,P[i][0],P[i][1],P[i][2]);



    //// Set up VTK arrays

    vtkCellArray* vtkCA = vtkCellArray::New();
    vtkCA->SetCells(Nt,vtkID);

    vtkUnstructuredGrid* vtkUG = vtkUnstructuredGrid::New();
    vtkUG->SetPoints(vtkP);
    vtkUG->SetCells(VTK_TETRA,vtkCA);

//        vtkUnstructuredGrid *vtkUG = vtkUnstructuredGrid::New();
//            vtkUG->SetPoints(vtkP);
//            vtkUG->SetCells(VTK_TETRA,ca);

	return vtkUG;
}


#endif /* CGALMESHTOVTKUNSTRUCTUREDGRID_HPP_ */
