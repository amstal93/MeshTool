#ifndef MESHPROPERTY_HPP
#define MESHPROPERTY_HPP

#include <array>
#include <map>
#include <vector>
#include <string>
#include <sstream>                                                              
#include <iostream>
#include <exception>

using namespace std;

struct MeshProperty{
    float facetAngle = 0.0;
    float facetSize = 0.0;
    float facetDistance = 0.0;
    float cellRadiusEdgeRatio = 0.0;
    vector<float> cellSize;
    int smoothFactor = 0;
    array<double,6> bbox_margin;  // (xmin,ymin,zmin,xmax,ymax,zmax)
    map<string, array<float,3>> perRegionOffset;

    MeshProperty (float facet_angle = 0.0,float facet_size = 0.0,float facet_distance = 0.0,
                  float cell_radius_edge_ratio = 0.0,vector<float> cell_size = vector<float>(), int smooth_factor = 0 ,array<double,6> bboxMargin = {0,0,0,0,0,0}):
                    facetAngle(facet_angle),
                    facetSize(facet_size),
                    facetDistance(facet_distance),
                    cellRadiusEdgeRatio(cell_radius_edge_ratio),
                    cellSize(cell_size),
                    smoothFactor(smooth_factor),
                    bbox_margin(bboxMargin) { }

    //
    // Parses a list of cellSizes into the cellSize vector (',' delimeter)
    //
    void cellSizeFromString(string& input) {
        // clear list of cellSizes
        cellSize.clear();

        // create a string stream to parse the input
        istringstream iss(input);

        string s;

        // parse on comma delimeters
        while(getline(iss, s, ',')) {
            // try to convert to a double
            // on failure default to 1.0
            try {
                double num = stod(s);
                cellSize.push_back(num);
            } catch(exception& ia) {
                cerr << "Unable to convert '" << s << "' to a double. Defaulting value to 1.0" << endl;
                cellSize.push_back(1.0);
            }
        }
    }

    //
    // Creates a string for the cellSize list separated by ','
    //
    string cellSizeToString() {
        string str = "";
        
        // build the string
        for(unsigned i = 0; i < cellSize.size(); i++) {
            str += to_string(cellSize[i]);
            if(i < cellSize.size()-1) {
                str += ",";
            }
        }
        
        return str;
    }
};
#endif // MESHPROPERTY_HPP
