#ifndef CGALPOLYHEDRONSURFACETOPOLYDATA_HPP
#define CGALPOLYHEDRONSURFACETOPOLYDATA_HPP

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>

#include <array>
#include <unordered_map>
using namespace std;

#include <boost/functional/hash.hpp>
#include <boost/range/adaptor/indexed.hpp>

template<class Polyhedron>
vtkPolyData* CGALPolyhedronSurfaceToPolyData(const Polyhedron& surf) {
    size_t Np = surf.size_of_vertices();
    size_t Nf = surf.size_of_facets();

    vtkPoints* vtkP = vtkPoints::New();
    vtkP->SetNumberOfPoints(Np);

    vtkIdType i=0;


    // use hash to map point coords into indices in a vector of distinct points

    unordered_map<array<float,3>,vtkIdType,boost::hash<array<float,3>>> pointToIDMap;

    boost::iterator_range<typename Polyhedron::Point_const_iterator> PR = boost::iterator_range<typename Polyhedron::Point_const_iterator>(
            surf.points_begin(), surf.points_end());

    for(const auto v : PR | boost::adaptors::indexed(0U))
    {
        typename Polyhedron::Point_3 p = v.value();
        std::array<float,3> pf{ float(p[0]), float(p[1]), float(p[2]) };
        vtkP->SetPoint(v.index(),pf.data());

        pointToIDMap.insert(make_pair(pf,v.index()));
    }

    vtkIdTypeArray* vtkID = vtkIdTypeArray::New();
    vtkID->SetNumberOfTuples(Nf*4);

    i=0;

    for(auto fIt = surf.facets_begin(); fIt != surf.facets_end(); ++fIt)
    {
        vtkID->SetTuple1(i++,3);
        assert(fIt->is_triangle());

        auto vIt = fIt->facet_begin();

        do {
            typename Polyhedron::Point_3 p = vIt->vertex()->point();
            std::array<float,3> pf{ float(p[0]), float(p[1]), float(p[2]) };

            const auto m = pointToIDMap.at(pf);
            vtkID->SetValue(i++,m);
            vIt++;
        }
        while (vIt != fIt->facet_begin());

    }

    vtkCellArray* vtkCA = vtkCellArray::New();
    vtkCA->SetCells(Nf,vtkID);

    vtkPolyData* vtkPD = vtkPolyData::New();
    vtkPD->SetPoints(vtkP);
    vtkPD->SetPolys(vtkCA);

    return vtkPD;
}

#endif
