#include "DirectoryHelper.hpp"
using  namespace std;

string basename(const string s)
{
    size_t lastSlash = s.find_last_of('/');

    size_t l = lastSlash == string::npos ? 0 : lastSlash+1;

    size_t firstDot = s.find_first_of('.',lastSlash);

    size_t n = firstDot == string::npos ? string::npos : firstDot-l;
    return s.substr(l,n);
}

string dir(string s){
    size_t lastSlash = s.find_last_of('/');

    return s.substr(0,lastSlash);
}
