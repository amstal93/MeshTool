#include "MeshQuality.hpp"

#include <vtkMeshQuality.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkDoubleArray.h>
#include <vtkCellData.h>
#include <vtkDataSet.h>
#include <vtkThreshold.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>

using namespace std;

static double GetAverage(vtkMeshQuality*,int);
static void meshQualityAssess(vtkUnstructuredGrid* );
static vector<vtkUnstructuredGrid* > splitRegions(vtkUnstructuredGrid*, map<int,string>regionOrder);

void meshQualityAssess(std::string in){
    cout << " ============= Mesh Quality Report ============= " << endl;
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkUnstructuredGridReader::New();
    reader->SetFileName(in.c_str());
    reader->Update();

    ///Get the quality report for entire mesh
    cout << "Mesh Quality Report for entire mesh : " << basename(in) << endl;
    meshQualityAssess(reader->GetOutput());

    cout<<" ================= Report End ================= " << endl;
}


void meshQualityAssess(string in, map<int,string> regionOrder){

    cout << " ============= Mesh Quality Report ============= " << endl;
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkUnstructuredGridReader::New();
    reader->SetFileName(in.c_str());
    reader->Update();

    ///Get the quality report for entire mesh
    cout << "Mesh quality report for entire mesh : " << basename(in) << endl;
    meshQualityAssess(reader->GetOutput());

    ///Get the quality report for every single region
    /// First, split out different regions
    cout << "Mesh quality report for single region " << endl;
    vector<vtkUnstructuredGrid* > regions = splitRegions(reader->GetOutput(),regionOrder);

    for(unsigned i = 0 ; i < regions.size(); i++){
        cout << i << ". " << regionOrder[i] << endl;
        meshQualityAssess(regions[i]);
    }

    cout<<" ================= Report End ================= " << endl;
}


static double GetAverage(vtkMeshQuality* meshQuality,int arg){
    meshQuality->SetTetQualityMeasure(arg);
    meshQuality->Update();

    vtkDoubleArray* qualityArray =
        vtkDoubleArray::SafeDownCast(meshQuality->GetOutput()->GetCellData()->GetArray("Quality"));

    int tN = qualityArray->GetNumberOfTuples();

    double sum = 0.0;
    for(vtkIdType i = 0; i < tN; i++)
    {
        double val = qualityArray->GetValue(i);
        sum += val;
    }

    return sum / tN;
}

static void meshQualityAssess(vtkUnstructuredGrid* ug){
    vtkSmartPointer<vtkMeshQuality> meshQuality  = vtkMeshQuality::New();
    meshQuality->SetInputData(ug);

    ///Compute the volume of mesh
    meshQuality->SetTetQualityMeasureToVolume();
    meshQuality->Update();
    vtkDoubleArray* qualityArray =
        vtkDoubleArray::SafeDownCast(meshQuality->GetOutput()->GetCellData()->GetArray("Quality"));

    int tN = qualityArray->GetNumberOfTuples();
    cout << "Cells Number : " << tN << endl;

    double volume = 0.0;
    for(vtkIdType i = 0; i < tN; i++)
    {
        double val = qualityArray->GetValue(i);
        volume += val;
    }
    cout << "Volume : " << volume << endl;

    ///Compute the average qualities of tetrahedron
    double avgEdgeRatio = GetAverage(meshQuality,VTK_QUALITY_EDGE_RATIO);
    cout << "Average edge ratio of tetrahedron : " << avgEdgeRatio << endl;

    double avgRadiusRatio = GetAverage(meshQuality,VTK_QUALITY_RADIUS_RATIO);
    cout << "Average radius ratio of tetrahedron : " << avgRadiusRatio << endl;

    double avgMinAngle = GetAverage(meshQuality,VTK_QUALITY_MIN_ANGLE);
    cout << "Average minimum angle of tetrahedron : " << avgMinAngle << endl;

}

static vector<vtkUnstructuredGrid* > splitRegions(vtkUnstructuredGrid* mesh, map<int,string>regionOrder){
    vector<vtkUnstructuredGrid* > regions;

    for(auto region: regionOrder){
//        cout << region.first << " : " << region.second << endl;

        vtkSmartPointer<vtkThreshold> threshold = vtkThreshold::New();
        threshold->SetInputData(mesh);
        threshold->SetInputArrayToProcess(0,0,0,vtkDataObject::FIELD_ASSOCIATION_CELLS,vtkDataSetAttributes::SCALARS);

        ///The value in the mesh starts from empty space of bounding box,
        /// so we use region order plus one to indicate the value of regions
        threshold->ThresholdBetween(region.first + 1,region.first + 1);
        threshold->Update();

        vtkSmartPointer<vtkUnstructuredGrid> ug = threshold->GetOutput();
//        vtkSmartPointer<vtkUnstructuredGridWriter> w = vtkUnstructuredGridWriter::New();
//            w->SetFileName((region.second +".mesh.vtk").c_str());
//            w->SetInputData(ug);
//            w->Update();
//            w->Delete();

        regions.push_back(ug);
    }

    return regions;
}

