#ifndef MESHWRITER_HPP
#define MESHWRITER_HPP

#include <string>

class MeshWriter
{
public:
    MeshWriter();

    void writeMesh(std::string in,std::string out);
};

#endif // MESHWRITER_HPP
